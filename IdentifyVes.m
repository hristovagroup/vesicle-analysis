function [Stats, Boundary, Vesicles]=IdentifyVes(Iraw)
% Identifies the vesicle(s),  for each channel determines the
% boundary line (in Boundary) and the center and the
% surface area for each vesicle
%
% Iraw - the image as read from the file
%
% stats - an array of structures with elements 'Area' and 'Centroid'
%         and dimensions (Number of vesicles) x (3), 3 is the number
%         of channels)
%
Boundary = {};

V0.Area = []; 
V0.Centroid = []; 
V0.DeltaR = [];
V0.Radius = [];
V0.Boundary = []; 

Vesicles = [V0 V0 V0]; clear V0; 

figure(10);  clf;
figure(11);  clf;
    for Nchannel=1:size(Iraw, 3);

        %remove outliers
        I = medfilt2(Iraw(:,:,Nchannel),[3 3]);

        %convert to black&white
        % maxI = max(max(I));
        threshold = graythresh(I/256);  % changed from 256 to 32
        bw = im2bw(I/256,threshold);    %changed from 256 to 32

        %% Step 3: Remove the noise
        % Using morphology functions, remove pixels which do not belong to the
        % objects of interest.

        % remove all object containing fewer than 200 pixels
        bw = bwareaopen(bw,200);         

        % fill a gap in the pen's cap
        se = strel('disk',2);
        bw = imclose(bw,se);  

        % fill any holes, so that regionprops can be used to estimate
        % the area enclosed by each of the boundaries
        bw = imfill(bw,'holes');  %figure(3); imshow(bw);

        %% Step 4: Find the boundaries
        % Concentrate only on the exterior boundaries.  Option 'noholes' will
        % accelerate the processing by preventing |bwboundaries| from searching
        % for inner contours.

        [B,L] = bwboundaries(bw,'noholes');

        LengthB = length(B);

        % Use |regionprops| to obtain estimates of the area for all of the objects.
        % Notice that the label matrix returned by |bwboundaries| can be
        % reused by |regionprops|.

        if(LengthB == 0)
            s=[]; 
        else
            s = regionprops(L,'Area','Centroid');
        end

        IndexBad = []; 
        for nB =1:LengthB
            [DeltaR, RangePhi,  Radius, phi] = IsCircular(B{nB}, s(nB).Centroid); 
            if (DeltaR > .08) 
                IndexBad = [IndexBad nB]; 
            end  
            s(nB).DeltaR = DeltaR; 
            s(nB).Radius = Radius(1);

            figure(10);
            subplot(3, length(B), length(B)*(Nchannel-1)+nB);
            plot(B{nB}(:,2), B{nB}(:,1), '.', ...
                s(nB).Centroid(1), s(nB).Centroid(2), 'x', ...
                Radius(1)*cos(phi)+s(nB).Centroid(1), Radius(1)*sin(phi)+s(nB).Centroid(2),  'r.', ...
                Radius(2)*cos(phi)+s(nB).Centroid(1), Radius(2)*sin(phi)+s(nB).Centroid(2),  'g.'); 
            set(gca, 'xlim', s(nB).Centroid(1) + 1.4*Radius(1)*[-1 1], ...
                     'ylim', s(nB).Centroid(2) + 1.4*Radius(2)*[-1 1])
            axis('square')
            title(['R=' num2str(Radius(1), 3) ' \delta\phi=' num2str(RangePhi, 3) ' \deltaR/R=' num2str(100*DeltaR, 3)]); 


           
        end % for nB =1:LengthB
        %pause(1)   %DW: commented out for faster runtime

        if(~isempty(IndexBad)) 
            B(IndexBad) = []; 
            s(IndexBad) = []; 
        end

        Boundary{Nchannel} = B;
        Stats{Nchannel} = s;

        % Test display of only well recognized vesicles 
        LengthB = length(B);
        for nB =1:LengthB
            [DeltaR, RangePhi,  Radius, phi] = IsCircular(B{nB}, s(nB).Centroid); 

            figure(11);
            subplot(3, length(B), length(B)*(Nchannel-1)+nB);
            plot(B{nB}(:,2), B{nB}(:,1), '.', ...
                s(nB).Centroid(1), s(nB).Centroid(2), 'x', ...
                Radius(1)*cos(phi)+s(nB).Centroid(1), Radius(1)*sin(phi)+s(nB).Centroid(2),  'r.', ...
                Radius(2)*cos(phi)+s(nB).Centroid(1), Radius(2)*sin(phi)+s(nB).Centroid(2),  'g.'); 
            set(gca, 'xlim', s(nB).Centroid(1) + 1.4*Radius(1)*[-1 1], ...
                     'ylim', s(nB).Centroid(2) + 1.4*Radius(2)*[-1 1])
            axis('square')
            title(['R=' num2str(Radius(1), 3) ' \delta\phi=' num2str(RangePhi, 3) ' \deltaR/R=' num2str(100*DeltaR, 3)]); 

        end
    end % for Nchannel=1:size(Iraw, 3);
    
    Btmp = Boundary; 
    Stmp = Stats; 
    
    NVesicles = 0; 
    for Nchannel=1:3
        LengthB = length(Boundary{Nchannel}); 
        
        for nB = 1:LengthB
            NVesicles = NVesicles + 1; 
            Vesicles(NVesicles, Nchannel).Area     = Stats{Nchannel}(nB).Area;
            Vesicles(NVesicles, Nchannel).Centroid = Stats{Nchannel}(nB).Centroid;
            Vesicles(NVesicles, Nchannel).DeltaR   = Stats{Nchannel}(nB).DeltaR;
            Vesicles(NVesicles, Nchannel).Radius   = Stats{Nchannel}(nB).Radius;
            Vesicles(NVesicles, Nchannel).Boundary = Boundary{Nchannel}{nB}; 
                        
            for nc2=(Nchannel+1):3 % Copy the data of the same vesicle from other channels 
                LB2 = length(Boundary{nc2});
                AccountedVesicles = []; 
                for nb2 =1:LB2 
                      C0 = Stats{Nchannel}(nB).Centroid;
                      C1 = Stats{nc2}(nb2).Centroid;
                      DistC0C1 = norm(C0 - C1); 
                      if(DistC0C1 < 10) % The centroids are less than 10 pixels apart, i.e. this is the same vesicle
                          Vesicles(NVesicles, nc2).Area = Stats{nc2}(nb2).Area;
                          Vesicles(NVesicles, nc2).Centroid = Stats{nc2}(nb2).Centroid;
                          Vesicles(NVesicles, nc2).DeltaR = Stats{nc2}(nb2).DeltaR;
                          Vesicles(NVesicles, nc2).Radius = Stats{nc2}(nb2).Radius;
                                                
                          Vesicles(NVesicles, nc2).Boundary = Boundary{nc2}{nb2}; 
                          
                          AccountedVesicles = [AccountedVesicles nb2];                           
                      end
                end
                Stats{nc2}(AccountedVesicles) = []; 
                Boundary{nc2}(AccountedVesicles) = [];
            end
            
        end
    end
    
    
    
    Boundary = Btmp;     
    Stats = Stmp;
    if (NVesicles == 0) Vesicles = []; return; end   
end % Function 



