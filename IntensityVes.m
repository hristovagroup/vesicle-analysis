function [Intensity,radius] = IntensityVes_v3(Iraw, Vesicle, filename, DeepestFolderName, plottitle)

% Vesicle is a 1x3 array of structures, each describing a vesicle in a
% separate channel

Intensity = zeros(2,3); % 2 methods x 3 channels
ParameterName=char('A=', 'x_0=', '\sigma=', 'B=', 'C=', 'I_1=', 'I_2=');
NRadialPoints = 64;
profile = zeros(NRadialPoints, 1);
[X,Y] = meshgrid(1:512, 1:512);

lsopts = optimset('Display','off');

C2 = NaN * zeros(3,2); % New centroids

for nChannel = 1:3
    if(~isempty(Vesicle(1, nChannel).Area))
        centroid = Vesicle(1, nChannel).Centroid ;
        R2 = (X - centroid(1)).^2 + (Y - centroid(2)).^2 ;

        Mask = (R2 < (Vesicle(1, nChannel).Radius + 5)^2);
        C2(nChannel, 1) = sum(sum(Mask .* Iraw(:,:,nChannel) .* X)) / sum(sum(Mask .* Iraw(:,:,nChannel)));
        C2(nChannel, 2) = sum(sum(Mask .* Iraw(:,:,nChannel) .* Y)) / sum(sum(Mask .* Iraw(:,:,nChannel)));
    

    end
end

FirstGood = NaN;
for nChannel = 1:3
    if(~isempty(Vesicle(1, nChannel).Area))
        FirstGood = nChannel; % if FirstGood is 2
        AllChannels = [1 2 3];
        SelectOtherThanFirstGood = (FirstGood ~=  [1 2 3]); % This prododuces [1 0 1]
        ChannelsOrderedFirstGood = [FirstGood AllChannels(SelectOtherThanFirstGood)]   ;
        % This produces [2 1 3]
        break;
    end
end

figure(4); clf;
for nChannel =  ChannelsOrderedFirstGood

    if(isempty(Vesicle(1, nChannel).Area)) % use the geometry of a good channel
        Vesicle(1, nChannel) = Vesicle(1, FirstGood);
        Vesicle(1, nChannel).GoodChannel = 0; % This is not a good channel
        C2(nChannel,:) = C2(FirstGood,:);

        R2 = (X - C2(nChannel,1)).^2 + (Y - C2(nChannel,2)).^2 ;
        Mask = (R2 < (Vesicle(1, nChannel).Radius + 5)^2);
        C2(nChannel, 1) = sum(sum(Mask .* Iraw(:,:,nChannel) .* X)) / sum(sum(Mask .* Iraw(:,:,nChannel)));
        C2(nChannel, 2) = sum(sum(Mask .* Iraw(:,:,nChannel) .* Y)) / sum(sum(Mask .* Iraw(:,:,nChannel)));
    else
        Vesicle(1, nChannel).GoodChannel = 1;
    end

    centroid = C2(nChannel,:) ; % Vesicle(1, nChannel).Centroid ;
    vradius  = Vesicle(1, nChannel).Radius ; % sqrt(Vesicle(1, nChannel).Area/pi);
    vperimeter = 2 * pi * vradius;

    %detecting hollow vesicles
    j=0:4;
    xo=centroid(1)+(vradius)*cos(double(pi*j/2));
    yo=centroid(2)+(vradius)*sin(double(pi*j/2));


    outer=mean(improfile(Iraw(:,:,nChannel),xo,yo)); %calculating an average intensity profile
 

    xi=centroid(1)+(vradius/2)*cos(double(pi*j/2));
    yi=centroid(2)+(vradius/2)*sin(double(pi*j/2));
    %calculating an average intensity profile
    inner=mean(improfile(Iraw(:,:,nChannel),xi,yi)) ;
  
    FilledVesicle = (inner > outer);

    nprofile=ceil(vperimeter);
    j=0:nprofile;
    Phase = double(2*pi*j/nprofile);

    xC2 = Vesicle(1,nChannel).Centroid(1) + (Vesicle(1,nChannel).Radius+5)*cos(Phase);
    yC2 = Vesicle(1,nChannel).Centroid(2) + (Vesicle(1,nChannel).Radius+5)*sin(Phase);

    if vradius>20
        InnerRadius = vradius - 20;
    else
        InnerRadius = 3;
    end
    dR = .5;
    for nR=1:NRadialPoints
        % generating a circle around vesicle center for intensity profile

        xi=centroid(1)+(InnerRadius+dR*nR)*cos(Phase);
        yi=centroid(2)+(InnerRadius+dR*nR)*sin(Phase);
        %calculating an average intensity profile
        profile(nR)=mean(improfile(Iraw(:,:,nChannel),xi,yi));
    end

    Ydata = profile(:);
    %removing possible NaNs (not-a-numbers)
    Ydata(isnan(Ydata))=[];
    Xdata=InnerRadius+dR*(1:length(Ydata));

    if (FilledVesicle)
        figure(4);
        % subplot(3, 3, 3*nChannel-2);
        subplot(3, 4, 4*nChannel-3);
        hold off; imshow(Iraw(:,:,nChannel)/256/16); hold on;
        plot(Vesicle(1,nChannel).Boundary(:,2), ...
            Vesicle(1,nChannel).Boundary(:,1), 'g', 'linewidth', 1); % Vesicle(1,nChannel).Boundary
        plot(centroid(1),centroid(2),'gx');
        title(['Ch:' num2str(nChannel) ' ' filename]) ;

        % subplot(3, 3, 3*nChannel-1);
        subplot(3, 4, 4*nChannel-2);
        hold off; imshow(Iraw(:,:,nChannel)/256/16); hold on;
        plot(Vesicle(1,nChannel).Boundary(:,2), Vesicle(1,nChannel).Boundary(:,1), 'g', 'linewidth', 1);
        plot(centroid(1),centroid(2),'gx', ...
            Vesicle(1,nChannel).Centroid(1), Vesicle(1,nChannel).Centroid(2), 'r+');
        plot(xC2, yC2, 'y');
        title(['Ch:' num2str(nChannel) ' ' filename]) ;
        set(gca, 'xlim', centroid(1) + (1.5*vradius)*[-1 1], ...
            'ylim', centroid(2) + (1.5*vradius)*[-1 1])

        % subplot(3, 3, 3*nChannel);
        subplot(3, 2, 2*nChannel);
        plot(Xdata(:), [Ydata(:)]);
        title('Filled Vesicle');
        axis('tight');
        grid;
        if (nChannel ==  ChannelsOrderedFirstGood(1))
            % First good is filled, cannot provide parameters
            Intensity = [];
            return;
        end
        continue ;
    end

    % Curve fitting

    if (Vesicle(1, nChannel).GoodChannel) % Added Apr 25, 2022
        % by ordering the channels in ChannelsOrderedFirstGood
        % it is ensured that the first iteration of the loop
        % will be with a good channel and that will generate
        % the paramters from the curve fit;
        % A channel that is not good will be approximated by a
        % gaussian with the same position and width sigma,
        % but with height to be determined,
        [MaxY IndY] = max(Ydata);
        params = lsqcurvefit(@gauss_erf, [MaxY Xdata(IndY) 1 mean(Ydata(1:10))], Xdata(:), Ydata(:), [], [], lsopts);
        params(5) = 0;
        % y = A exp(- (x - x0)^2/(2*\sigma^2)) + B * (1 - erf((x - x0)/(sqrt(2) \sigma ))) + C
        % params(1) -  A
        % params(2) - x0
        % params(3) - \sigma
        % params(4) - B
        % params(5) - C
        params_GoodChannel = params;
        % TheGaussian = 0 * Xdata(:);
    else
        TheGaussian = exp( -(Xdata(:) - params_GoodChannel(2)).^2 ./ (2 * params_GoodChannel(3).^2)  );
        TheErf =     (1 - erf((Xdata(:) - params_GoodChannel(2))./(sqrt(2)* params_GoodChannel(3))));

        fit_obj = fit([TheGaussian(:) TheErf(:)],  Ydata(:), 'poly11');
        params = params_GoodChannel;
        params(1) = fit_obj.p10;
        params(4) = fit_obj.p01;
        params(5) = fit_obj.p00;
    end

    radius=params(2);   

    PeakDetectable = (MaxY > 0.1) & ...  
        (params(3) < 5);   
    RadiusRange = params(2) + params(3) * [-3 3];
    if (RadiusRange(1)<=0) RadiusRange(1) = 2; end

    R2 = (X - centroid(1)).^2 + (Y - centroid(2)).^2 ;
    if(PeakDetectable)
        Intensity(1,nChannel)=params(1)*params(3)*(2*pi)^0.5;

        % Use a second method to calculate intensity
        Mask = (RadiusRange(1)^2 < R2 ) & (R2 < RadiusRange(2)^2);

        BackgroundFunction = params(4) * (1 - erf((sqrt(R2) - (params(2))) / params(3)/sqrt(2))) ...
            + params(5); 
        Temp = (squeeze(Iraw(:,:,nChannel)) - BackgroundFunction) .* Mask ;
        Intensity(2, nChannel) = sum(sum(Temp))/(2*pi*params(2));

    end



    xi=centroid(1)+(RadiusRange(1))*cos(Phase);
    yi=centroid(2)+(RadiusRange(1))*sin(Phase);

    xo=centroid(1)+(RadiusRange(2))*cos(Phase);
    yo=centroid(2)+(RadiusRange(2))*sin(Phase);

    xpeak=centroid(1)+(params(2))*cos(Phase);
    ypeak=centroid(2)+(params(2))*sin(Phase);

    figure(4);
    % subplot(3, 3, 3*nChannel-2);
    subplot(3, 4, 4*nChannel-3);
    hold off; imshow(Iraw(:,:,nChannel)/256/16); hold on;
    plot(Vesicle(1,nChannel).Boundary(:,2), Vesicle(1,nChannel).Boundary(:,1), 'g', 'linewidth', 1);
    plot(centroid(1),centroid(2),'gx');
    title(['Ch:' num2str(nChannel) ' ' filename]) ;

    % subplot(3, 3, 3*nChannel-1);
    subplot(3, 4, 4*nChannel-2);
    hold off; imshow(Iraw(:,:,nChannel)/256/16); hold on;
    plot(Vesicle(1,nChannel).Boundary(:,2), Vesicle(1,nChannel).Boundary(:,1), 'g', 'linewidth', 1);
    plot(xi, yi, 'r', xo, yo, 'r', xpeak, ypeak, 'b');
    plot(centroid(1),centroid(2),'gx', ...
        Vesicle(1,nChannel).Centroid(1), Vesicle(1,nChannel).Centroid(2), 'r+');
    plot(xC2, yC2, 'y');
    title(['Ch:' num2str(nChannel) ' ' filename]) ;
    set(gca, 'xlim', centroid(1) + (1.5*vradius)*[-1 1], ...
        'ylim', centroid(2) + (1.5*vradius)*[-1 1])

    % subplot(3, 3, 3*nChannel);
    subplot(3, 2, 2*nChannel);
    GaussianApproximation = gauss_erf(params, Xdata)+params(5);
    plot(Xdata, [Ydata(:) GaussianApproximation(:) (Ydata(:) - GaussianApproximation(:))]);
    axis('tight');
    text(1.02*Xdata(end), 0.5 * max(Ydata), ...
        [ParameterName num2str([params(:); Intensity(:, nChannel)/5.12], '%-6g')]);
    grid;

    sgtitle(plottitle);

end 




orient landscape
print('-dpsc', '-append',  [DeepestFolderName '_c2.ps']);


return ;

