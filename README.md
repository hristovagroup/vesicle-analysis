# vesicle analysis

This repository contains the Matlab code used for analysing fluorscent microscopy images of vesicles to extract membrane intensities as described in the paper **"Quantification of ligand and mutation-induced bias in EGFR phosphorylation in direct response to ligand binding"** by Daniel Wirth, Ece Özdemir and Kalina Hristova.


## Description
1) the script ROI_selector_deep_stage is run to identify good vesicles and create subimages only containing one good vesicle. The subimages are stored in *_ImageDataStore.mat
2)  the script vescile_v4 reads *_ImageDataStore.mat and extracts the membrane intensities of the vesciles


## License
The software is open source under the Apache-2.0 License. Feel free to use the software, but please cite its authors.
© 2020 The Johns Hopkins University, C16478
