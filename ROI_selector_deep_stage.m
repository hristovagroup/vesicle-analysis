% this script calls a trained object detector to detect good vesicles in 
% fluorescent microscope images from an automatic stage. The ROI, as well
% as the Image data of the good vesicle gets stored in the variable 
% ImageDataStore, which can be passed on to the vesicle program to get the 
% intensity of the vesicles. (20190723, DW)
clear;
user = getenv('UserName');
start_dir = strcat('J:\data\confocal\');
path = uigetdir(start_dir,'select the folder containing the images...');
cd(path);
DirName = pwd; 
DeepestFolderStart = find((pwd == filesep), 1, 'last')+1; 
DeepestFolderName = DirName(DeepestFolderStart:end); 

list = dir('**/*.*');
list = list([list(:).isdir]);
list = list(~ismember({list(:).name},{'.','..'}));

i = 1;
while i <= length(list)
    if isempty(strfind(list(i).name,'TileScan')) == 1
    list(i)=[];
    else 
        i = i + 1;
    end
end

storage = cell(1,length(list));
for i = 1:length(list)
    storage{1,i} = strcat(list(i).folder,'\',list(i).name);
end

imds = imageDatastore(storage,'IncludeSubfolders',true);

        
% edit the adress where the trained NN is stored
resnet = load('enter_your_storage_location\resnet18_mb=1.mat');
detector = resnet.detector;

% --- input original image size and input Size for neural net here ---
originalImageSize = [512 512 1];
inputSize = [224 224 3];
rescale = originalImageSize(1:2)./inputSize(1:2);

data = transform(imds,@(data)preprocessData(data,inputSize));
detectionResults = detect(detector,data,'ExecutionEnvironment','cpu','MinibatchSize',4);
[r c] = size(detectionResults);

%resize bboxes to original image size
for l = 1:r
detectionResults.Boxes{l,1} = bboxresize(detectionResults.Boxes{l,1},rescale);
end

%extract only the vesicle bboxes, scores, labels and put them in vesicle
%Result table
vesicleResults = table('Size',[r 4],'VariableTypes',{'cell','cell','cell','cell'},...
    'VariableNames',{'imageFilename','Boxes','Scores','Labels'});
for i = 1:r
    labels_temp = detectionResults.Labels{i,1};
    scores_temp = detectionResults.Scores{i,1};
    ves_idx = (labels_temp=='vesicle');
    labels_temp = labels_temp(ves_idx==1);
    scores_temp = scores_temp(ves_idx==1);
    boxes_ves = detectionResults.Boxes{i,1}(ves_idx==1,:);
    vesicleResults.Scores{i,1} = scores_temp;
    vesicleResults.Boxes{i,1} = boxes_ves;
    vesicleResults.Labels{i,1} = labels_temp;
    vesicleResults.imageFilename{i,1} = imds.Files(i,1);
end



%delete overlaping bboxes
for i = 1:r 
    if vesicleResults.Boxes{i,1}~[];
    [vesicleResults.selectedBoxes{i,1},vesicleResults.selectedScores{i,1},index] = selectStrongestBbox(vesicleResults.Boxes{i,1},...
        vesicleResults.Scores{i,1},'RatioType','Min','OverlapThreshold',0.5);
    vesicleResults.selectedLabels{i,1} = vesicleResults.Labels{i,1}(index);
    end
end

%add several pixels to each bounding box, to ensure the vesicle is in it
for i = 1:r  
    if vesicleResults.Boxes{i,1}~[];
    vesicleResults.selectedBoxes{i,1}(:,1) = vesicleResults.selectedBoxes{i,1}(:,1)-15;
    vesicleResults.selectedBoxes{i,1}(:,2) = vesicleResults.selectedBoxes{i,1}(:,2)-15;
    vesicleResults.selectedBoxes{i,1}(:,3) = vesicleResults.selectedBoxes{i,1}(:,3)+30;
    vesicleResults.selectedBoxes{i,1}(:,4) = vesicleResults.selectedBoxes{i,1}(:,4)+30;
    end
end

%print the ROIs to a .ps file for visual validation
progressbar
for k = 1:r
    I = imread(char(vesicleResults.imageFilename{k}));
    [r2 c] = size(vesicleResults.selectedBoxes{k,1});
    labels_temp = vesicleResults.selectedLabels{k,1};
    scores_temp = vesicleResults.selectedScores{k,1};
    for n = 1:r2
        annotation(n,1) = string(sprintf('%s (%f)', labels_temp(n,1), scores_temp(n,1)));
    end
    Iadj = imadjust(I);
    if vesicleResults.selectedBoxes{k,1}~[];
    I = insertObjectAnnotation(Iadj,'rectangle',vesicleResults.selectedBoxes{k,1},annotation,'LineWidth',1,'FontSize',8);
    annotation = string();
    printer = figure('Visible','Off');
    imshow(I)
    title(vesicleResults.imageFilename{k});
    print('-dpsc', '-append',  [DeepestFolderName '_ROI.ps']);
    else
      annotation = string();
    printer = figure('Visible','Off');
    imshow(Iadj)
    title(vesicleResults.imageFilename{k});
    print('-dpsc', '-append',  [DeepestFolderName '_ROI.ps']); 
    end
    close all
    progressbar(k/r)
end
    

ImageDataStore = cell(size(imds.Files,1)/3,1); 
counter = 1;
for k=1:r/3
    Iraw=importdata(char(vesicleResults.imageFilename{counter,1}));  %import Donorscan
    Iraw(:,:,2)=importdata(char(vesicleResults.imageFilename{counter+1,1}));  %import Acceptorscan
    Iraw(:,:,3)=importdata(char(vesicleResults.imageFilename{counter+2,1}));  %import FRETscan
    
   [n ~] = size(vesicleResults.selectedBoxes{counter,1});
   [n2 ~] = size(vesicleResults.selectedBoxes{counter+1,1});
   [n3 ~] = size(vesicleResults.selectedBoxes{counter+2,1});
    
    %store all the information in ImageDataStore
    %select ROI from channel with most detected vesicles
    if n>n2 & n>n3
    ImageDataStore{k,1}=struct('Name',imds.Files(counter,1),'Image',Iraw, 'ROI', vesicleResults.selectedBoxes{counter,1}, ...
        'Labels', vesicleResults.selectedLabels{counter,1}, 'Scores', vesicleResults.selectedScores{counter,1}, 'numberROI',n);
    elseif n2>n3
         ImageDataStore{k,1}=struct('Name',imds.Files(counter,1),'Image',Iraw, 'ROI', vesicleResults.selectedBoxes{counter+1,1}, ...
        'Labels', vesicleResults.selectedLabels{counter+1,1}, 'Scores', vesicleResults.selectedScores{counter+1,1}, 'numberROI',n2);
    else
         ImageDataStore{k,1}=struct('Name',imds.Files(counter,1),'Image',Iraw, 'ROI', vesicleResults.selectedBoxes{counter+2,1}, ...
        'Labels', vesicleResults.selectedLabels{counter+2,1}, 'Scores', vesicleResults.selectedScores{counter+2,1}, 'numberROI',n3);
    end
    
    counter = counter+3;    %update the counter
    
end

save(strcat(DeepestFolderName,'_ImageDataStore'),'ImageDataStore','-v7.3');
close all;

function data = preprocessData(data,targetSize)
% Resize image and bounding boxes to targetSize.
scale = targetSize(1:2)./size(data,[1 2]);
data = imresize(data,targetSize(1:2));

if targetSize(3) == 3
   data = cat(3, data, data, data); 
end

%data{2} = bboxresize(data{2},scale);
end