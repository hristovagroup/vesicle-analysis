% this script loads the subimages of identified good vesicles by 
% ROI_selector_deep_stage.m and gives the intensities of each vesicle 

clear;
warning('off', 'MATLAB:handle_graphics:exceptions:SceneNode');
start_dir = strcat('J:\data\confocal\');
path = uigetdir(start_dir,'select the folder containing the images...');
cd(path); 
DirName = path;
DeepestFolderStart = find((pwd == filesep), 1, 'last')+1; 
DeepestFolderName = DirName(DeepestFolderStart:end); 

% load the pictures into one big ImageDataStore from multiple
% ImageDataStore.mat files

filelist = ls(path+"\"+'*.mat'); %list all mat files in DeeepestFolderName 
for k = 1:size(filelist,1)
    temp(k,1) = load(filelist(k,:));
    if k==1
       ImageDataStore = temp(k).ImageDataStore;
    else
       ImageDataStore = vertcat(ImageDataStore, temp(k).ImageDataStore);
    end
end

progressbar                                                                                
for a=1:size(ImageDataStore,1)
    n = ImageDataStore{a}.numberROI;

for i=1:n
    try
    ROI_temp = imcrop(ImageDataStore{a}.Image(:,:,1), ImageDataStore{a}.ROI(i,:)); %stores the cropped Donor image
    ROI_temp(:,:,2) = imcrop(ImageDataStore{a}.Image(:,:,2), ImageDataStore{a}.ROI(i,:)); %stores the cropped Acceptor scan
    ROI_temp(:,:,3) = imcrop(ImageDataStore{a}.Image(:,:,3), ImageDataStore{a}.ROI(i,:)); %stores the cropped FRET scan
 
    
    %adjust image format
    [row,column]=size(ROI_temp(:,:,1));
    ROI_temp(row+1:512,column+1:512,:)=zeros;
    ROI_temp=double(ROI_temp);
    
    %get the perimeter
     [Stats, Boundary, V0]=IdentifyVes(ROI_temp);
     
     if isempty(V0)==0
         
     Vesicles = [];
     VesFile = [];
     DeepestFolderStart2 = find((ImageDataStore{a}.Name == filesep), 1, 'last')+1; 
     DeepestFolderName2 = ImageDataStore{a}.Name(DeepestFolderStart2:end); 
     [token, remain] = strtok(DeepestFolderName2,'--');
     token_temp = strtok(remain,'--');
     [token_front,remain_temp2]=strtok(token_temp);
     [token_temp2,token_end]=strtok(remain_temp2,'Series');
     filename = strcat(token_front,':',token_end,' ROI',num2str(i));
     Vesicles = [Vesicles; V0];  %#ok<AGROW>
     VesFile = strvcat(VesFile, filename(ones(size(V0, 1), 1), :));   %Which file did the vesicle come from
     plottitle = strcat(ImageDataStore{a}.Name,'ROI',num2str(i));
     [Intensity] = IntensityVes(ROI_temp, V0(1,:), filename, DeepestFolderName, plottitle);
     intensity_Donorscan_gaussian{i,1} = Intensity(1,1);
     intensity_Acceptorscan_gaussian{i,1} = Intensity(1,2);
     intensity_FRETscan_gaussian{i,1} = Intensity(1,3);
     intensity_Donorscan_perimeter{i,1} = Intensity(2,1);
     intensity_Acceptorscan_perimeter{i,1} = Intensity(2,2);
     intensity_FRETscan_perimeter{i,1} = Intensity(2,3);
    

   
  
    % export intensities to workspace
     check = exist('alldata');
     if check == 0
        alldata = [{'Name'},{'intensity_Donor_gaussian'},{'intensity_Acceptor_gaussian'},{'intensity_FRET_gaussian'},{'intensity_Donor_perimeter'},{'intensity_Acceptor_perimeter'},{'intensity_FRET_perimeter'}]; 
     end
     file_name = {strcat(ImageDataStore{a}.Name,'ROI',num2str(i));};
     datapoint = [file_name, intensity_Donorscan_gaussian{i,1},  intensity_Acceptorscan_gaussian{i,1}, intensity_FRETscan_gaussian{i,1}, intensity_Donorscan_perimeter{i,1},  intensity_Acceptorscan_perimeter{i,1}, intensity_FRETscan_perimeter{i,1}];
     alldata = [alldata; datapoint];  
     
     end
     
    
    catch 
         file_name = {strcat(ImageDataStore{a}.Name,'ROI',num2str(i));};
         disp(char(strcat({'Error in: '},file_name)))

    end
end
progressbar(a/size(ImageDataStore,1))
end


% write data to excel sheet
writecell(alldata,strcat(DeepestFolderName,' data.xlsx'),"Sheet",'alldata');

